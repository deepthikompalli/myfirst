import { Component, OnInit } from '@angular/core';
import{ServercallService} from "../servercall.service";
import {ShareDataService } from '../share-data.service';

@Component({
  selector: 'app-table-view',
  templateUrl: './table-view.component.html',
  styleUrls: ['./table-view.component.css']
})
export class TableViewComponent implements OnInit {
  
  data;
  heads = ["Id","Name","Age","Mobile","Email","Gender","Edit","Delete"];
  keys = ["id","name","age","mobile","email","gender"];
  constructor(private server:ServercallService,private shared:ShareDataService) {
    
   }

  ngOnInit() {
    this.fnGetEmployees();
    
  }

  fnGetEmployees():any{
    this.server.fnGetEmp("http://localhost:2121/employee/getEmp").subscribe((res)=>{
      debugger;
      this.data = res;
      this.shared.emp.next(this.data);
    })
  }

  fnDelete(rowdata):any{
    debugger;
    this.server.fnPostEmp("http://localhost:2121/employee/delEmp",rowdata.id).subscribe((res:any)=>{
      debugger;
      if(res.affectedRows == 1){
        alert("record deleted");
      }
    })
  }
  fnEdit(rowData):any{
      
       debugger;

       this.shared.bs.next(rowData)
  }

}
