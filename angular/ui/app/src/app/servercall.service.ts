import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ServercallService {

  constructor(private http:HttpClient) { }

  fnGetEmp(url):any{
    return this.http.get(url);
  }

  fnPostEmp(url,data):any{
    return this.http.post(url,data);
  }
}
