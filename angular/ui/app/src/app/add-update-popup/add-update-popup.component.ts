import { Component, OnInit, Input } from '@angular/core';
import {FormGroup,FormControl,Validators} from '@angular/forms';
import {ServercallService} from '../servercall.service';
import {ShareDataService} from '../share-data.service'

@Component({
  selector: 'app-add-update-popup',
  templateUrl: './add-update-popup.component.html',
  styleUrls: ['./add-update-popup.component.css']
})
export class AddUpdatePopupComponent implements OnInit {
   userData;
   employees;
  
   EmpData:FormGroup;
   
     
  constructor(private server:ServercallService,private shared:ShareDataService) {

    this.EmpData = new FormGroup({
      name : new FormControl("",[Validators.required]),
      age: new FormControl("",[Validators.required]),
      mobile : new FormControl("",[Validators.minLength(8),Validators.maxLength(10)]),
      email : new FormControl(),
      gender : new FormControl()
    })





   }

  ngOnInit() {
    this.shared.bs.subscribe((data)=>{
      debugger;
      JSON.stringify(this.EmpData.value);
     this.userData= data;
     this.EmpData.patchValue({
       name: this.userData.name, 
        age: this.userData.age,
        mobile : this.userData.mobile,
        email : this.userData.email,
        gender : this.userData.gender
    });
     

      
    })

    this.shared.emp.subscribe((employees)=>{
      this.employees = employees;
    })
  }

 

  addEmp(){
    
    this.server.fnPostEmp("http://localhost:2121/employee/addEmp",this.EmpData.value).subscribe((res:any)=>{
        if(res.affectedRows==1){
          this.employees.push(this.EmpData);
          alert("employee added successfully");

        }

     
    })
  }

  editEmp(){
    debugger;
    this.server.fnPostEmp("http://localhost:2121/employee/editEmp",this.EmpData.value).subscribe((res:any)=>{
       if(res.affectedRows==1){
         alert("record updated successfully");
       }
    })
  }



}
