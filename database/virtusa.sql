-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 06:50 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `virtusa`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(30) NOT NULL,
  `name` varchar(80) NOT NULL,
  `age` varchar(30) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(80) NOT NULL,
  `gender` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `age`, `mobile`, `email`, `gender`) VALUES
(2, 'muralii', '31', '9666634503', 'murali.nainala@gmail.com', 'male'),
(3, 'jathin', '4', '123456', 'jathin.nainala@gmail.com', 'male'),
(4, 'deepthi', '20', '9705298007', 'deepu.kompalli@gmail.com', 'female'),
(6, 'jhansi', '36', '9502559567', 'jhansik@gmail.com', 'female'),
(7, 'kiran', '38', '9908881148', 'srikirankompalli@gmail.com', 'male'),
(8, 'bhavani', '25', '9666304942', 'bhavaninainala@gmail.com', 'female'),
(10, 'hari', '26', '123456789', 'abcd@gmail.com', 'male'),
(11, 'Anitha', '34', '988667755', 'anitha.web@gmail.com', 'female'),
(12, 'siva', '39', '9888810781', 'siva@gmail.com', 'male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
